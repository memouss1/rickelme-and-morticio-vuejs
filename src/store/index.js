import { createStore } from 'vuex'

export default createStore({
  state: {
    // ! Definir  propiedad que va a contener a los personajes
    characters: [],
  },
  getters: {
  },
  mutations: {
    // ! Dedinir propiedad que va a modificar los states
    setCharacters(state, payload){
      state.characters = payload;
    }
  },
  actions: {
    // ! hacer petición a la API

    async getCharacters({commit}) {
      try {
        const resp = await fetch('https://rickandmortyapi.com/api/character');

        const data = await resp.json();
        console.log(data);

        commit('setCharacters', data.results)
        console.log(data.results);

      } catch (err) {
        console.log(err);
      }
    }

    //! Hacer un nuevo componente en la carpeta Components
  },
  modules: {
  }
})
